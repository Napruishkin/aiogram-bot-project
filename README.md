# Aiogram Bot Project

This is an aiogram-based Telegram bot project that allows you to create a bot with features like user registration, address management, and more.

## Table of Contents

- [Features](#features)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [Usage](#usage)
- [Project Structure](#project-structure)


## Features

- User registration and management.
- User-specific address management with one-to-many relationships.
- Command to retrieve user information and addresses.
- Modular code structure for easy maintenance.
- Customizable bot commands.

## Getting Started

Follow the instructions below to get started with this bot project.

### Prerequisites

Before you begin, ensure you have met the following requirements:

- Python 3.7+
- Pip (Python package manager)
- SQLite or another supported database system

### Installation

1. Clone this repository:

   ```bash
   git clone git@gitlab.com:Napruishkin/aiogram-bot-project.git
    ```
Navigate to the project directory:

```bash
cd aiogram-bot-project
```
Create and activate a virtual environment (recommended):

```bash
python -m venv venv

source venv/bin/activate  # On Windows, use: venv\Scripts\activate
```
### Install the project dependencies:

```bash
pip install -r requirements.txt
```

### Create the database tables:

```bash
python create_tables.py
```

Rename the .env.example file to .env and set your Telegram Bot Token:

```makefile
BOT_TOKEN=your_bot_token_here
```
### Start the bot:

```bash
python main.py
```

## Project Structure

The project is organized into the following modules:

- `handlers`: Contains message handlers for bot commands and user interactions.
- `middlewares`: Contains middleware for managing database sessions and bot commands.
- `keyboards`: Contains custom keyboard markup definitions.
- `service`: Contains service classes for working with the database and other utilities.
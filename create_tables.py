import asyncio

from sqlalchemy import create_engine

from database import Base

DATABASE_URL = "mysql+aiomysql://yourusername:yourpassword@localhost/yourdbname"

async def create_tables():
    engine = create_engine(DATABASE_URL, echo=True)

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


if __name__ == "__main__":
    asyncio.run(create_tables())

from sqlalchemy.future import select
from database import User, Address, AsyncSessionLocal


class UserService:
    @staticmethod
    async def create_user(user_id):
        async with AsyncSessionLocal() as session:
            user = User(user_id=user_id)
            session.add(user)
            await session.commit()

    @staticmethod
    async def add_address(user_id, address):
        async with AsyncSessionLocal() as session:
            user = await session.execute(select(User).where(User.user_id == user_id))
            user = user.scalar()
            if user:
                user.addresses.append(Address(address=address))
                await session.commit()

    @staticmethod
    async def get_user_info(user_id):
        async with AsyncSessionLocal() as session:
            user = await session.execute(select(User).where(User.user_id == user_id))
            user = user.scalar()
            if user:
                addresses = [address.address for address in user.addresses]
                return f"User ID: {user_id}\nAddresses: {', '.join(addresses)}"
            return "User not found"

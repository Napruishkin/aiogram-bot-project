from aiogram import Router
from aiogram.filters import Command, StateFilter

__all__: tuple[str] = ("register_user_handlers",)

from bot.handlers.user_handlers import start, add_address, process_address, get_info
from bot.middlewares.database_middleware import DatabaseMiddleware


def register_user_commands(router: Router) -> None:
    """Registers user handlers"""

    router.message.register(start, (Command(commands=["start"])))
    router.message.register(add_address, (Command(commands=["add_address"])))
    router.message.register(process_address, (StateFilter("add_address")))
    router.message.register(get_info, (Command(commands=["info"])))

    router.message.middleware(DatabaseMiddleware())


register_user_handlers = register_user_commands

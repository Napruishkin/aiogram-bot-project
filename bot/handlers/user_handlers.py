from aiogram import types
from aiogram.fsm.context import FSMContext

from bot.service.user_service import UserService


async def start(message: types.Message):
    user_id = message.from_user.id
    user_service = UserService()
    await user_service.create_user(user_id)
    await message.reply("Hello! You are registered in the system.", timeout=10)


async def add_address(message: types.Message, state: FSMContext):
    await message.reply("Enter your address:")
    await state.set_state("add_address")


async def process_address(message: types.Message, state: FSMContext):
    user_id = message.from_user.id
    address = message.text
    user_service = UserService()
    await user_service.add_address(user_id, address)
    await message.reply("Address added successfully.")
    await state.clear()


async def get_info(message: types.Message):
    user_id = message.from_user.id
    user_service = UserService()
    user_info = await user_service.get_user_info(user_id)
    await message.reply(user_info)

from aiogram.types import BotCommand

bot_commands = [
    BotCommand(command="help", description="Get help"),
    BotCommand(command="contacts", description="Get contact details"),
    BotCommand(command="settings", description="Access settings"),
]

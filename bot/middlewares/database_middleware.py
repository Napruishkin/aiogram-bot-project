from aiogram import BaseMiddleware
from bot.commands import bot_commands

from database import get_session


class DatabaseMiddleware(BaseMiddleware):
    async def on_pre_process_message(self, message, data):
        data['session'] = get_session()

        await message.bot.set_my_commands(bot_commands)

    async def __call__(self, message, data, tx):
        await super().__call__(message, data, tx)

from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

add_address_keyboard = ReplyKeyboardMarkup(
    keyboard=[
        [KeyboardButton(text="/add_address")],
    ],
    resize_keyboard=True,
)
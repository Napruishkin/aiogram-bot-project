from sqlalchemy import Column, Integer, String, ForeignKey, create_engine
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

DATABASE_URL = "mysql+aiomysql://yourusername:yourpassword@localhost/yourdbname"

engine = create_engine(DATABASE_URL, echo=True)

AsyncSessionLocal = sessionmaker(
    bind=engine,
    class_=AsyncSession,
    expire_on_commit=False,
)


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, unique=True, index=True)
    addresses = relationship("Address", back_populates="user")

    def __repr__(self):
        return f"User(id={self.id}, user_id={self.user_id})"


class Address(Base):
    __tablename__ = 'addresses'

    id = Column(Integer, primary_key=True, index=True)
    address = Column(String)
    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship("User", back_populates="addresses")

    def __repr__(self):
        return f"Address(id={self.id}, address={self.address}, user_id={self.user_id})"


def create_tables():
    Base.metadata.create_all(engine)


def get_session() -> Session:
    return sessionmaker(engine)()


async def close_database():
    await engine.dispose()
